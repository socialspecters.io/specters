# Specters

[![Pipeline Status](https://gitlab.com/socialspecters.io/specters/badges/main/pipeline.svg)](https://gitlab.com/socialspecters.io/specters/-/commits/main)
[![Coverage Report](https://gitlab.com/socialspecters.io/specters/badges/main/coverage.svg)](https://gitlab.com/socialspecters.io/specters/-/commits/main)
[![Go Report](https://img.shields.io/badge/go%20report-A+-brightgreen.svg?style=flat)](https://goreportcard.com/report/gitlab.com/socialspecters.io/specters)
[![License GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](./LICENSE)

> Serverless p2p backend for secure communications built on libp2p and [Textile ThreadsDB](https://github.com/textileio/go-threads)

## Table of Contents

* [Table of Contents](#table-of-contents)
* [Security](#security)
* [Install](#install)
    * [Daemon](#daemon)
    * [Client](#client)
* [Getting Started](#getting-started)
    * [Running Specters](#running-specters)
        * [Configuration values](#configuration-values)

## Security

Specters is still under heavy development and no part of it should be used before a thorough review of the underlying code and an understanding APIs and protocols may change rapidly.

Please also read the [security note](https://github.com/ipfs/go-ipfs#security-issues) for [go-ipfs](https://github.com/ipfs/go-ipfs).

## Install

This repo contains a daemon and client for interacting with these layers as a _remote service_.

### Daemon

-   **Build from the source**:

```bash
git clone https://gitlab.com/socialspecters.io/specters.git
cd specters
make build
./bin/specters
```

#### Client

```go
import "gitlab.com/socialspecters.io/specters/pkg/api/client"
```

## Getting Started

You can think of the [Specters' client](https://gitlab.com/socialspecters.io/specters/pkg/api/client) as a gRPC client wrapper around the internal `specters` package API. This section will only focus on getting started with the gRPC client, but Golang apps may choose to interact directly with `specters`.

### Running Specters

The `specters` daemon can be run as a server or alongside desktop apps or command-line tools. The easiest way to run `specters` is by using the provided Docker Compose files. If you're new to Docker and/or Docker Compose, get started [here](https://docs.docker.com/compose/gettingstarted/). Once you are set up, you should have `docker-compose` in your `PATH`.

Create an `.env` file and add the following values:

```bash
SPCTRS_REPO=/data/specters
SPCTRS_DEBUG=true
SPCTRS_MONGOURI=mongodb://mongo:27017
SPCTRS_MONGODATABASE=specters
```

Run it with the following command.

```bash
docker-compose up 
```

You should see some console output:

```bash
specters_1  | 2022-03-07T19:38:44.074Z  DEBUG   specters        specters/main.go:115    hostAddr: /ip4/0.0.0.0/tcp/4006
specters_1  | 2022-03-07T19:38:44.074Z  DEBUG   specters        specters/main.go:119    apiAddr: /ip4/0.0.0.0/tcp/6006
specters_1  | 2022-03-07T19:38:44.074Z  DEBUG   specters        specters/main.go:120    connLowWater: 100
specters_1  | 2022-03-07T19:38:44.074Z  DEBUG   specters        specters/main.go:121    connHighWater: 400
specters_1  | 2022-03-07T19:38:44.074Z  DEBUG   specters        specters/main.go:122    connGracePeriod: 20s
specters_1  | 2022-03-07T19:38:44.074Z  DEBUG   specters        specters/main.go:123    enableNetPubsub: false
specters_1  | 2022-03-07T19:38:44.074Z  DEBUG   specters        specters/main.go:125    mongoURI: mongodb://mongo:27017
specters_1  | 2022-03-07T19:38:44.074Z  DEBUG   specters        specters/main.go:126    mongoDatabase: specters
specters_1  | 2022-03-07T19:38:44.074Z  DEBUG   specters        specters/main.go:130    debug: true
specters_1  | 2022-03-07T19:38:44.074Z  INFO    specters        specters/main.go:132    ==> Specters server configuration:
specters_1  | 2022-03-07T19:38:44.074Z  INFO    specters        specters/main.go:133    
specters_1  | 2022-03-07T19:38:44.074Z  INFO    specters        specters/main.go:134            API Address: /ip4/0.0.0.0/tcp/6006
specters_1  | 2022-03-07T19:38:44.075Z  INFO    specters        specters/main.go:135             Go Version: go1.17.7
specters_1  | 2022-03-07T19:38:44.075Z  INFO    specters        specters/main.go:137                Storage: mongodb://mongo:27017
specters_1  | 2022-03-07T19:38:44.075Z  INFO    specters        specters/main.go:141                Version: 544249a77b0e3ba424ab74ad7c107a96ac4332cb
specters_1  | 2022-03-07T19:38:44.075Z  INFO    specters        specters/main.go:142              Log Level: debug
specters_1  | 2022-03-07T19:38:44.075Z  INFO    specters        specters/main.go:143    
specters_1  | 2022-03-07T19:38:44.075Z  INFO    specters        specters/main.go:144    ==> Welcome to Social Specters! Log data will stream in below:
```

Congrats! Now you have Specters running locally.

#### Configuration values

Note the various configuration values shown in the output above. These can be modified with environment variables show below.

- ***`SPCTRS_REPO`***: Repo location. Mandatory when launching from docker compose.
- ***`SPCTRS_HOSTADDR`***: Libp2p host bind address. `/ip4/0.0.0.0/tcp/4006` by default.
- ***`SPCTRS_APIADDR`***: gRPC Specters API bind address. `/ip4/0.0.0.0/tcp/6006` by default.
- ***`SPCTRS_CONNLOWWATER`***: Low watermark of libp2p connections that'll be maintained. `100` by default.
- ***`SPCTRS_CONNHIGHWATER`***: High watermark of libp2p connections that'll be maintained. `400` by default.
- ***`SPCTRS_CONNGRACEPERIOD`***: Duration a new opened connection is not subject to pruning. `20` seconds by default.
- ***`SPCTRS_ENABLENETPUBSUB`***: Enables thread networking over libp2p pubsub. `false` by default.
- ***`SPCTRS_DEBUG`***: Enables debug logging. `false` by default.
