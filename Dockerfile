FROM golang:1.17 as builder

WORKDIR /go/src/gitlab.com/socialspecters.io/specters

COPY . .

RUN CGO_ENABLED=0 make build

FROM alpine:latest

EXPOSE 4006
EXPOSE 6006

RUN addgroup -S specters && adduser -S specters -G specters

# Create the repo directory and switch to a non-privileged user.
ENV SPCTRS_PATH /data/specters
RUN mkdir -p $SPCTRS_PATH \
  && chown -R specters:specters $SPCTRS_PATH

# Switch to a non-privileged user
USER specters

# Expose the repo as a volume.
VOLUME $SPCTRS_PATH

WORKDIR /app

COPY --from=builder /go/src/gitlab.com/socialspecters.io/specters/bin/specters .
COPY --from=builder /go/src/gitlab.com/socialspecters.io/specters/bin/specters-cli .

ENTRYPOINT ["./specters"]

CMD ["--repo=/data/specters"]
