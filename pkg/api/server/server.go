package server

import (
	"context"
	"crypto/rand"
	"fmt"
	logging "github.com/ipfs/go-log/v2"
	"gitlab.com/socialspecters.io/specters/pkg/api/pb"
	"gitlab.com/socialspecters.io/specters/pkg/specters"
	"gitlab.com/socialspecters.io/specters/pkg/util"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"time"
)

var (
	loggingSetLogLevel    = logging.SetLogLevel
	randRead              = rand.Read
	log                   = logging.Logger("specters:api")
	tokenChallengeBytes   = 32
	tokenChallengeTimeout = time.Minute
)

// Server is a gRPC Specters API server.
type Server struct {
	pb.UnimplementedAPIServer
	Specters *specters.Specters
}

// Config specifies service settings.
type Config struct {
	Debug bool
}

// NewServer starts and returns a new server.
func NewServer(specters *specters.Specters, config Config) (*Server, error) {
	err := loggingSetLogLevel("specters:api", util.LevelFromDebugFlag(config.Debug))
	if err != nil {
		return nil, err
	}

	return &Server{
		Specters: specters,
	}, nil
}

// Status implements Status specters.pb.APIServer
func (s *Server) Status(ctx context.Context, in *pb.StatusRequest) (reply *pb.StatusReply, err error) {
	log.Debug("received status request")
	log.Debug("sending status response")

	return &pb.StatusReply{
		Initialized: s.Specters.IsInitialized(),
		Sealed:      s.Specters.IsSealed(),
		Shares:      specters.ShamirParts,
		Threshold:   specters.ShamirThreshold,
		Version:     s.Specters.Version(),
		ClusterID:   s.Specters.ClusterID(),
	}, nil
}

// Init implements Init specters.pb.APIServer
func (s *Server) Init(ctx context.Context, in *pb.InitRequest) (reply *pb.InitReply, err error) {
	log.Debug("received init request")

	parts, err := s.Specters.Init(in.RootPublicKey)
	if err != nil {
		return nil, err
	}

	log.Debug("sending init response")
	return &pb.InitReply{
		Keys:      parts,
		Threshold: specters.ShamirThreshold,
	}, nil
}

// Unseal implements Unseal specters.pb.APIServer
func (s *Server) Unseal(ctx context.Context, in *pb.UnsealRequest) (reply *pb.UnsealReply, err error) {
	log.Debug("received unseal request")

	err = s.Specters.Unseal(in.Key)
	if err != nil {
		return nil, err
	}

	log.Debug("sending unseal response")
	return &pb.UnsealReply{
		Initialized: s.Specters.IsInitialized(),
		Sealed:      s.Specters.IsSealed(),
		Shares:      specters.ShamirParts,
		Threshold:   specters.ShamirThreshold,
		Progress:    int32(s.Specters.UnsealProgress()),
		Version:     s.Specters.Version(),
		ClusterID:   s.Specters.ClusterID(),
	}, err
}

// GetToken implements GetToken specters.pb.APIServer
func (s *Server) GetToken(server pb.API_GetTokenServer) (err error) {
	log.Debug("received get token request")

	var key string
	req, err := server.Recv()
	if err != nil {
		return
	}
	switch payload := req.Payload.(type) {
	case *pb.GetTokenRequest_Key:
		key = payload.Key
	default:
		return status.Error(codes.InvalidArgument, "Key is required")
	}

	specter, err := s.Specters.GetSpecterByPubKey(key)
	if err != nil {
		return status.Error(codes.Unauthenticated, fmt.Sprintf("Error authenticating identity: %v", err))
	}
	if specter == nil {
		return status.Error(codes.Unauthenticated, fmt.Sprintf("Error authenticating identity"))
	}

	msg := make([]byte, tokenChallengeBytes)
	if _, err = randRead(msg); err != nil {
		return
	}
	timeoutCtx, cancel := context.WithTimeout(server.Context(), tokenChallengeTimeout)
	defer cancel()
	err = server.Send(&pb.GetTokenReply{
		Payload: &pb.GetTokenReply_Challenge{
			Challenge: msg,
		},
	})
	if err != nil {
		return
	}

	done := make(chan error)
	go func() {
		defer close(done)
		var err error
		req, err = server.Recv()
		if err != nil {
			done <- err
			return
		}
	}()
	select {
	case <-timeoutCtx.Done():
		return status.Error(codes.DeadlineExceeded, "Challenge deadline exceeded")
	case err, ok := <-done:
		if ok {
			return err
		}
	}

	var signature []byte
	switch payload := req.Payload.(type) {
	case *pb.GetTokenRequest_Signature:
		signature = payload.Signature
	default:
		return status.Error(codes.InvalidArgument, "Signature is required")
	}

	if ok, err := specter.Verify(msg, signature); !ok || err != nil {
		return status.Error(codes.InvalidArgument, "Bad signature")
	}

	tokenString, err := s.Specters.GetToken(specter)
	if err != nil {
		return
	}

	log.Debug("sending get token response")
	return server.Send(&pb.GetTokenReply{
		Payload: &pb.GetTokenReply_Token{
			Token: tokenString,
		},
	})
}
