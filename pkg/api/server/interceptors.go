package server

import (
	"context"
	"gitlab.com/socialspecters.io/specters/pkg/specters"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func getFilteredMethods() map[string]bool {
	return map[string]bool{
		"/specters.pb.API/Status": true,
		"/specters.pb.API/Init":   true,
		"/specters.pb.API/Unseal": true,
	}
}

// UnaryStatusInterceptor returns a new unary server interceptors that returns an error if the server is sealed.
func UnaryStatusInterceptor(s *specters.Specters) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		if _, ok := getFilteredMethods()[info.FullMethod]; !ok {
			if !s.IsInitialized() {
				return nil, status.Error(codes.FailedPrecondition, "Specters is not initialized")
			} else if s.IsSealed() {
				return nil, status.Error(codes.FailedPrecondition, "Specters is sealed")
			}
		}

		return handler(ctx, req)
	}
}

// StreamStatusInterceptor returns a new streaming server interceptor that returns an error if the server is sealed.
func StreamStatusInterceptor(s *specters.Specters) grpc.StreamServerInterceptor {
	return func(srv interface{}, stream grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
		if _, ok := getFilteredMethods()[info.FullMethod]; !ok {
			if !s.IsInitialized() {
				return status.Error(codes.FailedPrecondition, "Specters is not initialized")
			} else if s.IsSealed() {
				return status.Error(codes.FailedPrecondition, "Specters is sealed")
			}
		}

		err := handler(srv, stream)
		return err
	}
}
